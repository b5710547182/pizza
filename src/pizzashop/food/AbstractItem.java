package pizzashop.food;

import java.util.ArrayList;

public abstract class AbstractItem implements OrderItem {
	
	private int size;
	
	public AbstractItem(int size)
	{
		this.size = size;
	}
	public int getSize()
	{
		return size;
	}

}
