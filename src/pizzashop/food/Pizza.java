package pizzashop.food;
import java.util.*;

import pizzashop.*;
/** 
 * A pizza, of course.
 */
public class Pizza extends AbstractItem {
	// constants related to drink size
	public static final double [] prices = { 0.0, 120.0, 200.0, 300.0 };
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	/** list of toppings on this pizza */
	private List<Topping> toppings;
	private int size;
	
	/** A new pizza.
	 * @param size is 1 for small, 2 for medium, 3 for large.
	 */
	public Pizza( int size ) {
		super(size);
		toppings = new ArrayList<Topping>();
	}

	/* String description of this pizza */
	public String toString() {
		StringBuffer tops = new StringBuffer();
		if ( toppings.size() == 0 || 
				(toppings.size()==1 && toppings.get(0) == Topping.NONE ) ) 
			/* don't print anything */;
		else {
			tops.append(" with ");
			tops.append(toppings.get(0).toString());
			for( int k = 1; k<toppings.size(); k++ ) {
				tops.append(", ");
				tops.append( toppings.get(k).toString() );
			}
		}
	
		return String.format("%s pizza%s", sizes[this.getSize()], tops.toString() );
	}

	/* @see pizzashop.FoodItem#getPrice() */
	public double getPrice() {
		double price = 0;
		if ( this.getSize() >= 0 && this.getSize() < prices.length ) price = prices[this.getSize()];
		//TODO: add price of toppings
		for(int i =0 ; i < toppings.size();i++)
		{
			price += toppings.get(i).getPrice();
		}
		
		
		return price;
	}

	
	
	
	/** 
	 * add a topping
	 * @param topping is a topping to add to pizza
	 */
	public void add(Topping topping) {
		toppings.add( topping );
	}
	
	public void remove(Topping topping) {
		toppings.remove(topping);
	}
	
	public List<Topping> getToppings( ) {
		return toppings;
	}
	
	public void setToppings( List<Topping> toppings ) {
		this.toppings = toppings;
	}
	
	public Object clone() {
		Pizza clone = null;
		try {
			clone = (Pizza) super.clone();
			clone.size = this.size;
			// copy toppings into a new List in clone
			clone.toppings = new ArrayList<Topping>();
			for(Topping t : toppings ) clone.toppings.add(t);
		} catch (CloneNotSupportedException e) {
			System.out.println("Pizza.clone: " + e.getMessage() );
		}
		
		return clone;
	}
}
